package gamepackage.map;

import java.awt.geom.Line2D;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 29.08.13
 * Time: 14:38
 * To change this template use File | Settings | File Templates.
 */
public class Line extends Line2D.Double
{
  public Line()
  {

  }


  public Line(double x1, double y1, double x2, double y2)
  {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }


  public void setX1(double value)
  {
    this.x1 = value;
  }


  public void setY1(double value)
  {
    this.y1 = value;
  }


  public void setX2(double value)
  {
    this.x2 = value;
  }


  public void setY2(double value)
  {
    this.y2 = value;
  }
}
