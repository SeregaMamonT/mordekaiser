package gamepackage.map;

import gamepackage.gameobject.creature.MonsterType;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 29.08.13
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */
public class MapReader
{
  private static Map<Integer, String> TILE_TYPE_MAPPING = new HashMap<Integer, String>();


  static {
    TILE_TYPE_MAPPING.put(0, "emptyTile");
    TILE_TYPE_MAPPING.put(1, "filledTile");
    TILE_TYPE_MAPPING.put(2, "topRightTile");
    TILE_TYPE_MAPPING.put(3, "topLeftTile");
    TILE_TYPE_MAPPING.put(4, "botLeftTile");
    TILE_TYPE_MAPPING.put(5, "botRightTile");
  }


  private ClassPathXmlApplicationContext tileContext;


  public LevelModel readLevelModel(String fileName)
  {
    LevelModel levelModel = new LevelModel();
    try {
      Element root = new SAXBuilder().build(fileName).getRootElement();

      Element heroRespawn = root.getChild("hero-respawn");
      levelModel.setHeroX(Integer.parseInt(heroRespawn.getAttributeValue("x")));
      levelModel.setHeroY(Integer.parseInt(heroRespawn.getAttributeValue("y")));
      levelModel.setRespawns(readRespawns(root.getChild("respawns").getChildren()));
    }
    catch (JDOMException | IOException e) {
      throw new RuntimeException(e);
    }
    return levelModel;
  }


  private List<Respawn> readRespawns(List<Element> respawnList)
  {
    List<Respawn> respawns = new LinkedList<>();
    for (Element item : respawnList) {
      respawns.add(readRespawn(item));
    }
    return respawns;
  }


  private Respawn readRespawn(Element item)
  {
    double x = Double.parseDouble(item.getAttributeValue("x"));
    double y = Double.parseDouble(item.getAttributeValue("y"));
    int capacity = Integer.parseInt(item.getAttributeValue("capacity"));
    int timespan = Integer.parseInt(item.getAttributeValue("timespan"));
    Respawn resp = new Respawn(x, y, capacity, timespan);

    List<Element> monsterTypeElements = item.getChild("monsters").getChildren();
    List<MonsterType> monsterTypes = new ArrayList<MonsterType>();
    for (Element element : monsterTypeElements) {
      MonsterType monsterType = new MonsterType();
      monsterType.setMonsterName(element.getAttributeValue("name"));
      monsterType.setSkinName(element.getAttributeValue("skin"));
      monsterTypes.add(monsterType);
    }
    resp.setMonsterTypes(monsterTypes);
    return resp;
  }


  public Tile[][] readLandscape(String fileName)
  {
    BufferedReader reader = null;
    try {
      reader = new BufferedReader(new FileReader(fileName));

      String[] size = reader.readLine().split(" ");
      int rows = Integer.parseInt(size[0]);
      int cols = Integer.parseInt(size[1]);

      Tile[][] tiles = new Tile[rows][cols];

      for (int i = 0; i < rows; i++) {
        String[] words = reader.readLine().split(" ");

        for (int j = 0; j < words.length; j++) {
          int tileType = Integer.parseInt(words[j]);
          tiles[i][j] = createTile(i, j, tileType);
        }
      }
      return tiles;
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      if (reader != null) {
        try {
          reader.close();
        }
        catch (IOException ignore) { }
      }
    }
  }


  private Tile createTile(int row, int col, int type)
  {
    String tileName = TILE_TYPE_MAPPING.get(type);

    Tile tile = (Tile) getTileContext().getBean(tileName);
    tile.setType(type);
    tile.setRow(row);
    tile.setCol(col);
    tile.refreshLines();

    return tile;
  }


  private ClassPathXmlApplicationContext getTileContext()
  {
    if (tileContext == null) {
      tileContext = new ClassPathXmlApplicationContext("tiles.xml");
    }
    return tileContext;
  }
}
