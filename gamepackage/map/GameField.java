package gamepackage.map;

import java.util.LinkedList;

import gamepackage.gameobject.BasicObject;
import gamepackage.gameobject.creature.*;
import gamepackage.technical.NumericUtil;
import gamepackage.technical.Point;

public class GameField
{
  protected LevelModel levelModel;
  protected Tile[][] tiles;

  protected Hero hero;
  protected LinkedList<Monster> monsters = new LinkedList<>();
  protected LinkedList<Creature> creatures = new LinkedList<>();
  protected LinkedList<Respawn> respawns = new LinkedList<>();


  public GameField(int level)
  {
    readField(level);
  }


  private void readField(int levelCode)
  {
    LevelResources levelResources = new LevelResources(levelCode);

    MapReader mapReader = new MapReader();
    tiles = mapReader.readLandscape(levelResources.getMapPath());

    levelModel = mapReader.readLevelModel(levelResources.getLevelModelPath());
    for (Respawn resp : levelModel.getRespawns()) {
      addRespawn(resp);
    }
  }


  public void addRespawn(Respawn respawn)
  {
    respawns.add(respawn);
    respawn.setField(this);
  }


  public void setHero(Hero hero)
  {
    this.hero = hero;
    hero.setField(this);
    hero.setLocation(levelModel.getHeroX(), levelModel.getHeroY());
    creatures.add(hero);
  }


  public boolean isMovingAllowed(Creature creature, double cos, double sin)
  {
    double dx = creature.getSpeed() * cos;
    double dy = creature.getSpeed() * sin;

    return !intersects(creature, creature.getX() + dx, creature.getY() + dy);
  }


  public boolean intersects(Creature obj, double x, double y)
  {
    return (intersectsField(x, y, obj.getRadius()) ||
        intersectsMonsters(x, y, obj) || intersectsCreatures(x, y, obj));
  }


  public void addMonster(Monster monster)
  {
    monsters.add(monster);
  }


  public void removeCreature(Monster creature)
  {
    monsters.remove(creature);
  }


  public void removeCreature(Creature creature)
  {
    creatures.remove(creature);
  }


  public boolean intersectsMonsters(double x, double y, Creature creature)
  {
    for (Monster monster : monsters) {
      if (monster != creature && monster.intersects(x, y, creature.getRadius()))
        return true;
    }
    return false;
  }


  public boolean intersectsCreatures(double x, double y, Creature creature)
  {
    for (Creature item : creatures) {
      if (item != creature && item.intersects(x, y, creature.getRadius()))
        return true;
    }
    return false;
  }


  public boolean intersectsField(double x, double y, double radius)
  {
    int top = ((int) (y - radius)) / Tile.HEIGHT;
    int bottom = ((int) (y + radius)) / Tile.HEIGHT;
    int left = ((int) (x - radius)) / Tile.WIDTH;
    int right = ((int) (x + radius)) / Tile.WIDTH;

    for (int i = top; i <= bottom; i++) {
      for (int j = left; j <= right; j++) {
        if (tiles[i][j].intersects(x, y, radius))
          return true;
      }
    }
    return false;
  }


  public double getDistanceToHero(BasicObject obj)
  {
    double d = NumericUtil.distance(obj.getX(), obj.getY(), hero.getX(), hero.getY());
    return d - hero.getRadius() - obj.getRadius();
  }


  public Point getDirectionToHero(Point sender)
  {
    return NumericUtil.direction(sender.getX(), sender.getY(), hero.getX(), hero.getY());
  }


  public Hero getHero()
  {
    return hero;
  }


  public void update(long elapsedTime)
  {
    for (Monster monster : monsters)
      monster.update(elapsedTime);

    for (Respawn resp : respawns)
      resp.update(elapsedTime);
  }
}
