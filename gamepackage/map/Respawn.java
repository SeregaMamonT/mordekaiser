/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gamepackage.map;

import java.util.LinkedList;
import java.util.List;

import gamepackage.gameobject.DrawableObject;
import gamepackage.gameobject.creature.CreatureFactory;
import gamepackage.gameobject.creature.Monster;
import gamepackage.gameobject.creature.MonsterType;
import gamepackage.skin.ModelType;
import gamepackage.skin.Skin;
import gamepackage.skin.SkinFactory;

import java.util.*;

/**
 * @author User
 */
public class Respawn extends DrawableObject
{
  private int capacity;
  private long timespan;
  private long lastResp;
  private Random rand;

  private Monster waitingMonster;

  private LinkedList<Monster> monsters;
  private List<MonsterType> monsterTypes = null;


  public Respawn(double x, double y, int capacity, int timespan)
  {
    setLocation(x, y);

    this.capacity = capacity;
    this.timespan = timespan;

    monsters = new LinkedList<>();

    Skin skin = SkinFactory.getSkin("respawn");
    setModel(skin.getModel(ModelType.RESPAWN));

    lastResp = System.currentTimeMillis();
    rand = new Random();
  }


  public void update(long elapsedTime)
  {
    super.updateMovement(elapsedTime);

    if ((System.currentTimeMillis() - lastResp) > timespan)
      if (monsters.size() < capacity)
        generateMonster();
  }


  private void generateMonster()
  {
    if (waitingMonster == null) {
      MonsterType monsterType = monsterTypes.get(rand.nextInt(monsterTypes.size()));

      waitingMonster = CreatureFactory.getMonster(monsterType, getX(), getY(), field);
    }

    if (!field.intersects(waitingMonster, getX(), getY())) {
      monsters.add(waitingMonster);
      waitingMonster.setRespawn(this);
      field.addMonster(waitingMonster);
      lastResp = System.currentTimeMillis();
      waitingMonster = null;
    }
  }


  public void setMonsterTypes(List<MonsterType> monsterTypes)
  {
    this.monsterTypes = monsterTypes;
  }


  public void removeMonster(Monster monster)
  {
    monsters.remove(monster);
  }

}
