package gamepackage.map;

import java.util.LinkedList;
import java.util.List;

/**
 * User: salexetovich
 * Date: 14.07.14
 */
public class LevelModel
{
  private int heroX;
  private int heroY;
  private List<Respawn> respawns = new LinkedList<>();


  public int getHeroX()
  {
    return heroX;
  }


  public void setHeroX(int heroX)
  {
    this.heroX = heroX;
  }


  public int getHeroY()
  {
    return heroY;
  }


  public void setHeroY(int heroY)
  {
    this.heroY = heroY;
  }


  public void addRespawn(Respawn respawn)
  {
    respawns.add(respawn);
  }


  public List<Respawn> getRespawns()
  {
    return respawns;
  }


  public void setRespawns(List<Respawn> respawns)
  {
    this.respawns = respawns;
  }
}
