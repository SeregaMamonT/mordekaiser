package gamepackage.map;

/**
 * User: salexetovich
 * Date: 17.07.14
 */
public class LevelResources
{
  private final String levelName;
  private final String levelDirectory;

  public LevelResources(int levelCode)
  {
    this.levelName = String.format("level%02d", levelCode);
    this.levelDirectory = "resources/maps/" + levelName;
  }


  public String getMapPath()
  {
    return levelDirectory + "/map.txt";
  }


  public String getLevelModelPath()
  {
    return levelDirectory + "/" + levelName + ".xml";
  }


  public String getTilesPath()
  {
    return levelDirectory + "/tiles.png";
  }
}
