package gamepackage.map;

import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.background.TileBackground;
import gamepackage.ToolSet;
import gamepackage.gameobject.creature.Creature;
import gamepackage.gameobject.creature.Hero;
import gamepackage.gameobject.creature.Monster;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * User: salexetovich
 * Date: 17.07.14
 */
public class ClientGameField extends GameField
{
  private Background backgr;

  public ClientGameField(int level)
  {
    super(level);
    LevelResources levelResources = new LevelResources(level);
    BufferedImage[] tileImages = ToolSet.getInstance().getImageTool().getImages(levelResources.getTilesPath(), 6, 1);

    initBackground(tileImages, tiles.length, tiles[0].length);
  }


  @Override
  public void addRespawn(Respawn respawn)
  {
    super.addRespawn(respawn);
    respawn.setBackground(backgr);
  }


  @Override
  public void setHero(Hero hero)
  {
    super.setHero(hero);
    hero.setBackground(backgr);
  }


  @Override
  public void addMonster(Monster monster)
  {
    super.addMonster(monster);
    monster.setBackground(backgr);
  }


  private void initBackground(BufferedImage[] tileImages, int rows, int cols)
  {
    int[][] imageNumbers = new int[cols][rows];

    for (int i = 0; i < rows; i++)
      for (int j = 0; j < cols; j++)
        imageNumbers[j][i] = tiles[i][j].getType();

    backgr = new TileBackground(tileImages, imageNumbers);

    if (hero != null)
      hero.setBackground(backgr);

    for (Monster monster : monsters)
      monster.setBackground(backgr);

    for (Creature creature : creatures)
      creature.setBackground(backgr);

    for (Respawn resp : respawns)
      resp.setBackground(backgr);
  }


  public Creature onLocation(double x, double y)
  {
    gamepackage.technical.Point point = new gamepackage.technical.Point(x, y);

    for (Monster monster : monsters)
      if (point.distance(monster.getX(), monster.getY()) < monster.getRadius())
        return monster;

    for (Creature creature : creatures)
      if (point.distance(creature.getX(), creature.getY()) < creature.getRadius())
        return creature;

    return null;
  }


  @Override
  public void update(long elapsedTime)
  {
    super.update(elapsedTime);
    backgr.update(elapsedTime);
    backgr.setToCenter(hero);
  }


  public void render(Graphics2D g)
  {
    backgr.render(g);

    for (Respawn resp : respawns)
      resp.render(g);

    for (Monster monster : monsters)
      monster.render(g);
  }


  public double getX()
  {
    return backgr.getX();
  }


  public double getY()
  {
    return backgr.getY();
  }
}
