package gamepackage.map;

import java.util.*;

/**
 * Class representing map tile
 *
 * @author MamonT
 */
public class Tile
{
  private int type;
  private int row;
  private int col;

  public static final int WIDTH = 64;
  public static final int HEIGHT = 64;

  private List<Line> lines = new LinkedList<Line>();
  private List<Line> scales;


  public boolean intersects(double x, double y, double radius)
  {
    for (Line line : lines) {
      if (line.ptSegDist(x, y) < radius)
        return true;
    }
    return false;
  }


  public void refreshLines()
  {
    lines.clear();
    for (Line scale : scales) {
      double x1 = (col + scale.getX1()) * WIDTH;
      double x2 = (col + scale.getX2()) * WIDTH;
      double y1 = (row + scale.getY1()) * HEIGHT;
      double y2 = (row + scale.getY2()) * HEIGHT;
      lines.add(new Line(x1, y1, x2, y2));
    }
  }


  public void setScales(List<Line> scales)
  {
    this.scales = scales;
  }


  public int getType()
  {
    return type;
  }


  public void setType(int type)
  {
    this.type = type;
  }


  public void setCol(int column)
  {
    this.col = column;
  }


  public void setRow(int row)
  {
    this.row = row;
  }
}
