package gamepackage;

import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 29.08.13
 * Time: 18:09
 * To change this template use File | Settings | File Templates.
 */
public class ImageTool
{
  public BufferedImage[] getImages(String fileName, int cols, int rows)
  {
    return ToolSet.getInstance().getGame().getImages(fileName, cols, rows);
  }


  public BufferedImage getImage(String fileName)
  {
    return ToolSet.getInstance().getGame().getImage(fileName);
  }
}
