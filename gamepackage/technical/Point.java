package gamepackage.technical;

/**
 * Created with IntelliJ IDEA.
 * User: MamonT
 * Date: 13.02.14
 * Time: 23:42
 * To change this template use File | Settings | File Templates.
 */
public class Point
{
  private double x;
  private double y;


  public Point()
  {
  }


  public Point(double x, double y)
  {
    setX(x);
    setY(y);
  }


  public double getX()
  {
    return x;
  }


  public void setX(double x)
  {
    this.x = x;
  }


  public double getY()
  {
    return y;
  }


  public void setY(double y)
  {
    this.y = y;
  }


  public double distance(Point point)
  {
    return NumericUtil.distance(this.x, this.y, point.getX(), point.getY());
  }


  public double distance(double x, double y)
  {
    return NumericUtil.distance(this.x, this.y, x, y);
  }
}
