package gamepackage.technical;

/**
 * Created with IntelliJ IDEA.
 * User: MamonT
 * Date: 13.02.14
 * Time: 23:52
 * To change this template use File | Settings | File Templates.
 */
public class NumericUtil
{
  public static double distance(double x1, double y1, double x2, double y2)
  {
    return Math.sqrt(distance2(x1, y1, x2, y2));
  }


  public static double distance(Point point1, Point point2)
  {
    return distance(point1.getX(), point1.getY(), point2.getX(), point2.getY());
  }


  public static double distance2(double x1, double y1, double x2, double y2)
  {
    return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
  }


  public static double distance2(Point point1, Point point2)
  {
    return distance2(point1.getX(), point1.getY(), point2.getX(), point2.getY());
  }


  public static Point direction(double fromX, double fromY, double toX, double toY)
  {
    double deltaX = toX - fromX;
    double deltaY = toY - fromY;
    double d = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

    return d > 0 ? new Point(deltaX / d, deltaY / d) : new Point(0, 0);
  }

}
