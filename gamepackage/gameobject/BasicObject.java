package gamepackage.gameobject;

import com.golden.gamedev.object.AnimatedSprite;
import gamepackage.map.GameField;

/**
 * User: salexetovich
 * Date: 21.07.14
 */
public class BasicObject extends AnimatedSprite implements GameObject
{
  protected int uid;
  protected GameField field;
  protected double centerX = 0;
  protected double centerY = 0;
  protected double radius;


  @Override
  public double getX()
  {
    return super.getX() + centerX;
  }


  @Override
  public double getY()
  {
    return super.getY() + centerY;
  }


  public void setField(GameField field)
  {
    this.field = field;
  }


  public double getRadius()
  {
    return radius;
  }


  public void setRadius(double radius)
  {
    this.radius = radius;
  }


  @Override
  public int getUid()
  {
    return uid;
  }


  @Override
  public int setUid()
  {
    return uid;
  }
}
