package gamepackage.gameobject;

/**
 * Created with IntelliJ IDEA.
 * User: SAlexetovich
 * Date: 13.02.14
 * Time: 16:54
 * To change this template use File | Settings | File Templates.
 */
public interface GameObject
{
  double getX();

  double getY();

  double getRadius();

  int getUid();

  int setUid();
}
