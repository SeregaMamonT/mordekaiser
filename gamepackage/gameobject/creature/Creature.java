package gamepackage.gameobject.creature;

import gamepackage.gameobject.DrawableObject;
import gamepackage.skin.Model;
import gamepackage.skin.ModelMode;
import gamepackage.skin.ModelType;
import gamepackage.skin.Skin;
import gamepackage.technical.NumericUtil;


public abstract class Creature extends DrawableObject implements Attacker, Attackable
{
  protected double attackRange;
  protected double speed;
  protected Skin skin;
  protected int direction = -1;
  protected double AD;
  protected double basicAD;
  protected double HP;
  protected double maxHP;
  protected double basicHP;
  protected boolean canShoot;
  protected long lastAttack;
  protected long attackDelay;
  protected long lastMove;
  protected long stopDelay = 200;
  protected long moveCicle = 1000;
  protected ModelMode modelMode;
  public final String name;


  public Creature(String name)
  {
    super();
    this.name = name;
    lastAttack = System.currentTimeMillis() - attackDelay;
  }


  public void setSkin(Skin skin)
  {
    this.skin = skin;

    setModel(skin.getModel(ModelType.LEFT_MOVE));
  }


  @Override
  public void setModel(Model model)
  {
    setImages(model.getImages());
    setAnimate(true);

    if (model.getModelType().getMode() == ModelMode.ATTACK) {
      getAnimationTimer().setDelay(400 / model.getImages().length);
      modelMode = ModelMode.ATTACK;
      setLoopAnim(false);
    }
    else {
      getAnimationTimer().setDelay(moveCicle / model.getImages().length);
      modelMode = ModelMode.MOVE;
      setLoopAnim(true);
    }

    setX(getX() - model.getCenterX());
    setY(getY() - model.getCenterY());

    centerX = model.getCenterX();
    centerY = model.getCenterY();
  }


  public boolean intersects(double x, double y, double radius)
  {
    double d = NumericUtil.distance2(x, y, getX(), getY());
    double r = (this.radius + radius) * (this.radius + radius);
    return d < r;
  }


  @Override
  public void move(double cos, double sin)
  {
    super.move(speed * cos, speed * sin);
    lastMove = System.currentTimeMillis();
  }


  public void setDirection(double cos, double sin)
  {
    int newDir = cos > 0 ? 1 : -1;

    if (newDir != direction) {
      direction = newDir;

      switch (direction) {
        case 1:
          setModel(skin.getModel(ModelType.RIGHT_MOVE));
          break;
        case -1:
          setModel(skin.getModel(ModelType.LEFT_MOVE));
          break;
      }
    }
  }


  public boolean canAttack(Attackable obj)
  {
    if (obj == this)
      return false;
    return NumericUtil.distance(obj.getX(), obj.getY(), getX(), getY()) < attackRange + getRadius();
  }


  public void attack(Attackable target)
  {
    if (isReadyToAttack()) {
      lastAttack = System.currentTimeMillis();
      target.takeDamage(this, AD);

      if (direction == -1)
        setModel(skin.getModel(ModelType.LEFT_ATTACK));
      else
        setModel(skin.getModel(ModelType.RIGHT_ATTACK));
    }
  }


  protected boolean isReadyToAttack()
  {
    return System.currentTimeMillis() - lastAttack > attackDelay;
  }


  public void takeDamage(Attacker attacker, double damage)
  {
    HP -= damage;
    if (HP < 0.5) {
      field.removeCreature(this);
    }
  }


  @Override
  public void update(long elapsedTime)
  {
    super.update(elapsedTime);

    long curTime = System.currentTimeMillis();

    if (modelMode == ModelMode.ATTACK) {
      if (curTime - lastAttack > attackDelay)
        if (direction == -1)
          setModel(skin.getModel(ModelType.LEFT_MOVE));
        else
          setModel(skin.getModel(ModelType.RIGHT_MOVE));
    }
    else {
      setAnimate(curTime - lastMove <= stopDelay);
    }
  }


  public double getSpeed()
  {
    return speed;
  }


  public void setSpeed(double speed)
  {
    this.speed = speed;
  }


  public void setAD(double AD)
  {
    this.AD = AD;
  }


  public double getAD()
  {
    return AD;
  }


  public void setBasicAD(double basicAD)
  {
    this.basicAD = basicAD;
  }


  public double getBasicAD()
  {
    return basicAD;
  }


  public double getAttackRange()
  {
    return attackRange;
  }


  public void setAttackRange(double range)
  {
    attackRange = range;
  }


  public void setHP(double HP)
  {
    this.HP = HP;
  }


  public double getHP()
  {
    return HP;
  }


  public void setMaxHP(double maxHP)
  {
    this.maxHP = maxHP;
  }


  public double getMaxHP()
  {
    return maxHP;
  }


  public void setBasicHP(double basicHP)
  {
    this.basicHP = basicHP;
  }


  public double getBasicHP()
  {
    return basicHP;
  }


  public void setAttackDelay(long delay)
  {
    this.attackDelay = delay;
  }


  public long getAttackDelay()
  {
    return attackDelay;
  }


  public void setCanShoot(boolean canShoot)
  {
    this.canShoot = canShoot;
  }


  public boolean getCanShoot()
  {
    return canShoot;
  }

}