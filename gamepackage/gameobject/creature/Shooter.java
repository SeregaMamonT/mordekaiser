package gamepackage.gameobject.creature;

import gamepackage.gameobject.Projectile;
import gamepackage.skin.ModelType;
import gamepackage.technical.Point;

import java.awt.*;
import java.util.Iterator;
import java.util.LinkedList;

public class Shooter extends Monster
{
  protected LinkedList<Projectile> projectiles = new LinkedList<>();


  public Shooter(String name)
  {
    super(name);
  }


  public void attack(Attackable target)
  {
    if (isReadyToAttack()) {
      lastAttack = System.currentTimeMillis();
      Projectile projectile = new Projectile(target, this, AD, new Point(getX(), getY()), skin.getModel(ModelType.PROJECTILE));
      projectile.setBackground(getBackground());
      projectiles.add(projectile);
    }
  }


  @Override
  public void update(long elapsedTime)
  {
    super.update(elapsedTime);

    for (Iterator<Projectile> it = projectiles.iterator(); it.hasNext();) {
      Projectile projectile = it.next();
      projectile.update(elapsedTime);
      if (projectile.isReached())
        it.remove();
    }
  }


  public void render(Graphics2D g)
  {
    super.render(g);
    for (Projectile projectile : projectiles) {
      projectile.render(g);
    }
  }

}
