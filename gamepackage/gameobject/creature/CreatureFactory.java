package gamepackage.gameobject.creature;

import java.io.*;

import gamepackage.map.GameField;
import gamepackage.skin.SkinFactory;
import org.jdom.*;
import org.jdom.input.SAXBuilder;

public class CreatureFactory
{

  public static Hero getHero(String name, String skin)
  {
    try {
      Hero creature = readHero(name);

      creature.setSkin(SkinFactory.getSkin(skin));
      creature.setLvl(1);

      return creature;
    }
    catch (IOException e) {
      return null;
    }
  }


  public static Monster getMonster(MonsterType monsterType, double x, double y, GameField field)
  {
    try {
      Monster monster = readMonster(monsterType.getMonsterName());

      monster.setField(field);
      monster.setLocation(x, y);
      monster.setSkin(SkinFactory.getSkin(monsterType.getSkinName()));

      return monster;
    }
    catch (IOException e) {
      return null;
    }
  }


  private static Hero readHero(String name) throws IOException
  {
    try {
      Element root = new SAXBuilder().build("characters/" + name + ".xml").getRootElement();

      Hero creature = new Hero(name);

      creature.setRadius(Double.parseDouble(root.getAttributeValue("radius")));
      creature.setAttackRange(Double.parseDouble(root.getAttributeValue("attackRange")));
      creature.setSpeed(Double.parseDouble(root.getAttributeValue("speed")));
      creature.setBasicAD(Double.parseDouble(root.getAttributeValue("basicAD")));
      creature.setBasicXP(Integer.parseInt(root.getAttributeValue("basicXP")));
      double hp = Double.parseDouble(root.getAttributeValue("basicHP"));
      creature.setBasicHP(hp);
      creature.setHP(hp);
      creature.setAttackDelay(Long.parseLong(root.getAttributeValue("attackDelay")));
      creature.setCanShoot(Boolean.parseBoolean(root.getAttributeValue("canShoot")));

      return creature;
    }
    catch (JDOMException e) {
      return null;
    }
  }


  private static Monster readMonster(String name) throws IOException
  {
    try {
      Element root = new SAXBuilder().build("characters/" + name + ".xml").getRootElement();

      Monster creature;
      if (root.getAttributeValue("canShoot").equals("true"))
        creature = new Shooter(name);
      else
        creature = new Monster(name);

      creature.setDirectionDelay(Long.parseLong(root.getAttributeValue("directionDelay")));
      creature.setRadius(Double.parseDouble(root.getAttributeValue("radius")));
      creature.setAttackRange(Double.parseDouble(root.getAttributeValue("attackRange")));
      creature.setSpeed(Double.parseDouble(root.getAttributeValue("speed")));
      creature.setBasicAD(Double.parseDouble(root.getAttributeValue("basicAD")));
      creature.setAD(creature.getBasicAD());
      double hp = Double.parseDouble(root.getAttributeValue("basicHP"));
      creature.setBasicHP(hp);
      creature.setMaxHP(hp);
      creature.setHP(hp);
      creature.setAttackDelay(Long.parseLong(root.getAttributeValue("attackDelay")));
      creature.setCanShoot(Boolean.parseBoolean(root.getAttributeValue("canShoot")));

      return creature;
    }
    catch (JDOMException e) {
      return null;
    }
  }

}