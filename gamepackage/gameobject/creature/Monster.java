package gamepackage.gameobject.creature;

import gamepackage.ToolSet;
import gamepackage.map.Respawn;
import gamepackage.technical.Point;

public class Monster extends Creature
{
  protected long directionChanged;
  protected long directionDelay;
  protected Respawn respawn;
  protected Point moveDirection;
  protected double viewRadius = 200;


  public Monster(String name)
  {
    super(name);
  }


  public void move()
  {
    if (System.currentTimeMillis() - directionChanged > directionDelay) {
      if (field.getDistanceToHero(this) < viewRadius) {
        moveDirection = field.getDirectionToHero(new Point(getX(), getY()));
      }
      else {
        double angle = ToolSet.getInstance().getRandom().nextInt(360) / Math.PI;
        moveDirection = new Point(Math.cos(angle), Math.sin(angle));
        directionChanged = System.currentTimeMillis();
      }
    }
    setDirection(moveDirection.getX(), moveDirection.getY());
    if (field.isMovingAllowed(this, moveDirection.getX(), moveDirection.getY())) {
      move(moveDirection.getX(), moveDirection.getY());
    }
  }


  public void setDirectionDelay(long delay)
  {
    directionDelay = delay;
  }


  @Override
  public void takeDamage(Attacker attacker, double damage)
  {
    AttackResult attackResult = new AttackResult();
    attackResult.setTarget(this);

    HP -= damage;
    attackResult.setActualDamage(damage);

    if (HP < 0.5) {
      if (field != null)
        field.removeCreature(this);
      if (respawn != null)
        respawn.removeMonster(this);
      attackResult.setKilled(true);
    }
    attacker.notifyAttackResult(attackResult);
  }


  public void setRespawn(Respawn respawn)
  {
    this.respawn = respawn;
  }


  public Respawn getRespawn()
  {
    return respawn;
  }


  public void update(long elapsedTime)
  {
    super.update(elapsedTime);

    Hero hero = field.getHero();

    if (canAttack(hero))
      attack(field.getHero());
    else
      move();
  }


  @Override
  public void notifyAttackResult(AttackResult result)
  {
    // no action
  }
}
