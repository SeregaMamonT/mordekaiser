package gamepackage.gameobject.creature;

/**
 * Created with IntelliJ IDEA.
 * User: SAlexetovich
 * Date: 13.02.14
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
public class AttackResult
{
  private Attackable target;
  private boolean isKilled;
  private double actualDamage;


  public Attackable getTarget()
  {
    return target;
  }


  public void setTarget(Attackable target)
  {
    this.target = target;
  }


  public boolean isKilled()
  {
    return isKilled;
  }


  public void setKilled(boolean killed)
  {
    isKilled = killed;
  }


  public double getActualDamage()
  {
    return actualDamage;
  }


  public void setActualDamage(double actualDamage)
  {
    this.actualDamage = actualDamage;
  }
}
