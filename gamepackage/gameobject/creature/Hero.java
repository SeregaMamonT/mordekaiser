package gamepackage.gameobject.creature;

public class Hero extends Creature
{
  protected long XP;
  protected long basicXP;
  protected int lvl;
  protected double magickDamage = 0;


  public Hero(String name)
  {
    super(name);
  }


  public void takeXP(int XP)
  {
    this.XP += XP;
    if (this.XP >= getToLvlXP()) {
      this.XP = this.XP % getToLvlXP();
      setLvl(lvl + 1);
    }
  }


  public long getXP()
  {
    return XP;
  }


  public void setXP(long XP)
  {
    this.XP = XP;
  }


  public long getBasicXP()
  {
    return basicXP;
  }


  public void setBasicXP(long basicXP)
  {
    this.basicXP = basicXP;
  }


  public void setLvl(int lvl)
  {
    this.lvl = lvl;

    AD = basicAD * Math.pow(1.2, lvl - 1);
    maxHP = basicHP * Math.pow(1.2, lvl - 1);
    HP = maxHP;
  }


  public int getLvl()
  {
    return lvl;
  }


  public long getToLvlXP()
  {
    return (int) (basicXP * Math.pow(1.2, lvl - 1));
  }


  public double getMagickDamage()
  {
    return magickDamage;
  }


  public void setMagicDamage(double magicDamage)
  {
    this.magickDamage = magicDamage;
  }


  @Override
  public void notifyAttackResult(AttackResult result)
  {
    Creature target = (Creature) result.getTarget();
    if (result.isKilled()) {
      takeXP((int) Math.sqrt(target.getAD() * target.getMaxHP()));
    }
  }
}
