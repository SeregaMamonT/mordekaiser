package gamepackage.gameobject.creature;

import gamepackage.gameobject.GameObject;

/**
 * Created with IntelliJ IDEA.
 * User: SAlexetovich
 * Date: 13.02.14
 * Time: 16:11
 * To change this template use File | Settings | File Templates.
 */
public interface Attacker extends GameObject
{
  public boolean canAttack(Attackable target);

  public void attack(Attackable target);

  public void notifyAttackResult(AttackResult result);
}
