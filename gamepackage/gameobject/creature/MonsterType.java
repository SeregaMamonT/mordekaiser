package gamepackage.gameobject.creature;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 29.08.13
 * Time: 16:57
 * To change this template use File | Settings | File Templates.
 */
public class MonsterType
{
  private String monsterName;
  private String skinName;


  public String getMonsterName()
  {
    return monsterName;
  }


  public void setMonsterName(String monsterName)
  {
    this.monsterName = monsterName;
  }


  public String getSkinName()
  {
    return skinName;
  }


  public void setSkinName(String skinName)
  {
    this.skinName = skinName;
  }
}
