package gamepackage.gameobject.creature;

import gamepackage.gameobject.GameObject;

/**
 * Created with IntelliJ IDEA.
 * User: SAlexetovich
 * Date: 13.02.14
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
public interface Attackable extends GameObject
{
  public void takeDamage(Attacker attacker, double damage);
}
