package gamepackage.gameobject;

import gamepackage.gameobject.creature.AttackResult;
import gamepackage.gameobject.creature.Attackable;
import gamepackage.gameobject.creature.Attacker;
import gamepackage.skin.Model;
import gamepackage.technical.NumericUtil;
import gamepackage.technical.Point;

public class Projectile extends DrawableObject implements Attacker
{
  protected double speed = 3.5;
  protected double AD;
  protected Attackable target;
  protected Attacker sender;
  protected Model model;
  private boolean reached = false;


  public Projectile(Attackable target, Attacker sender, double basicDamage, Point position, Model model)
  {
    this.target = target;
    this.sender = sender;
    this.model = model;
    this.AD = basicDamage;

    setLocation(position.getX(), position.getY());

    setModel(model);
  }


  @Override
  public void move(double cos, double sin)
  {
    moveX(cos * speed);
    moveY(sin * speed);
  }


  public void setSpeed(double speed)
  {
    this.speed = speed;
  }


  public double getSpeed()
  {
    return speed;
  }


  @Override
  public void update(long elapsedTime)
  {
    super.update(elapsedTime);

    if (canAttack(target)) {
      attack(target);
      setReached(true);
    }
    else {
      Point direction = NumericUtil.direction(getX(), getY(), target.getX(), target.getY());
      move(direction.getX(), direction.getY());
    }

  }


  public boolean isReached()
  {
    return reached;
  }


  public void setReached(boolean reached)
  {
    this.reached = reached;
  }


  @Override
  public boolean canAttack(Attackable target)
  {
    double d = NumericUtil.distance2(target.getX(), target.getY(), getX(), getY());
    return d < target.getRadius() * target.getRadius();
  }


  @Override
  public void attack(Attackable target)
  {
    target.takeDamage(this, AD);
  }


  @Override
  public void notifyAttackResult(AttackResult result)
  {
    sender.notifyAttackResult(result);
  }
}
