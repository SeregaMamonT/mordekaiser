package gamepackage.gameobject;


import gamepackage.skin.Model;

public class DrawableObject extends BasicObject
{
  protected void setModel(Model model)
  {
    setImages(model.getImages());

    setX(getX() - model.getCenterX());
    setY(getY() - model.getCenterY());

    centerX = model.getCenterX();
    centerY = model.getCenterY();

    setAnimate(true);
    setLoopAnim(true);
  }


  @Override
  public double getScreenX()
  {
    return super.getScreenX() + centerX;
  }


  @Override
  public double getScreenY()
  {
    return super.getScreenY() + centerY;
  }
}
