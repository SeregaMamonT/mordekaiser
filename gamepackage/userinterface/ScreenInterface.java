package gamepackage.userinterface;

import com.golden.gamedev.Game;
import com.golden.gamedev.object.GameFont;
import gamepackage.ToolSet;
import gamepackage.gameobject.creature.Creature;
import gamepackage.gameobject.creature.Hero;

import java.awt.*;

public class ScreenInterface
{
  private Game game;
  private GameFont font;
  private boolean showHeroStats = false;


  public ScreenInterface(GameFont font)
  {
    this.game = ToolSet.getInstance().getGame();
    this.font = font;
  }


  public void showHeroStats(Graphics2D g, Hero hero)
  {
    if (!showHeroStats) return;

    int width = 350;
    int height = 400;
    int left = game.getWidth() - width - 20;
    int top = 40;
    int fontHeight = font.getHeight();
    int fontWidth = font.getWidth(' ');

    g.setStroke(new BasicStroke(10));
    g.setColor(Color.yellow);
    g.fillRoundRect(left, top, width, height, 20, 20);
    g.setColor(Color.orange);
    g.drawRoundRect(left, top, width, height, 20, 20);

    int right = left + width - 20;
    left += 20;
    top += 20;

    font.drawString(g, "DAMAGE:", left, top);
    String text = "MACE";
    font.drawString(g, text, left + fontWidth, top + (int) (fontHeight * 1.5));
    text = Integer.toString((int) hero.getAD());
    font.drawString(g, text, right - font.getWidth(text), top + (int) (fontHeight * 1.5));

    text = "SIPHON";
    font.drawString(g, text, left + fontWidth, top + (fontHeight * 3));
    text = Integer.toString((int) hero.getMagickDamage());
    font.drawString(g, text, right - font.getWidth(text), top + (fontHeight * 3));

    text = "HEALTH";
    font.drawString(g, text, left, top + (fontHeight * 5));
    text = Integer.toString((int) hero.getHP()) + "/" + Integer.toString((int) hero.getMaxHP());
    font.drawString(g, text, right - font.getWidth(text), top + (fontHeight * 5));

    text = "EXPIRIENCE";
    font.drawString(g, text, left, top + (fontHeight * 7));
    text = Integer.toString((int) hero.getXP()) + "/" + Integer.toString((int) hero.getToLvlXP());
    font.drawString(g, text, right - font.getWidth(text), top + (fontHeight * 7));
  }


  public void drawHealth(Graphics2D g, Creature creature)
  {
    if (creature == null)
      return;

    int width = font.getWidth(creature.name) + 10;
    int height = font.getHeight() + 10;

    int x = (game.getWidth() - width) / 2;
    int y = 10;

    g.setColor(Color.yellow);
    g.fillRect(x, y, width, height);
    double part = creature.getHP() / creature.getMaxHP();
    g.setColor(Color.orange);
    g.fillRect(x, y, (int) (width * part), height);

    font.drawString(g, creature.name.toUpperCase(), x + 5, y + 5);
  }


  public void drawHeroPanel(Graphics2D g, Hero hero)
  {
    int width = 300;
    int height = 75;
    int radius = 25;
    int fontHeight = font.getHeight();

    //отрисовка панели
    g.setColor(Color.yellow);
    g.fillRoundRect(0, 0, width, height, radius, radius);
    g.setColor(Color.orange);
    g.setStroke(new BasicStroke(5));
    g.drawRoundRect(0, 0, width, height, radius, radius);

    //вывод имени, уровня
    font.drawString(g, hero.name.toUpperCase(), 5, 10);
    String lvl = Integer.toString(hero.getLvl()) + " LVL";
    font.drawString(g, lvl, width - font.getWidth(lvl) - 10, 10);
    font.drawString(g, "HP", 7, 12 + (fontHeight + 4));
    font.drawString(g, "SH", 7, 12 + (fontHeight + 4) * 2);

    int lineWidth = width - 60;
    g.setStroke(new BasicStroke(2));

    //отрисовка полоски ХП
    double part = hero.getHP() / hero.getMaxHP();
    g.setColor(Color.red);
    g.fillRect(50, 14 + (fontHeight + 4), (int) (lineWidth * part), 10);
    g.drawRect(50, 14 + (fontHeight + 4), lineWidth, 10);
    //отрисовка полоски щита
    g.setColor(Color.gray);
    g.drawRect(50, 14 + (fontHeight + 4) * 2, lineWidth, 10);
    //g.fillRect(50, 14 + (fontHeight + 4) * 2, lineWidth, 10);

    width = game.getWidth();
    height = game.getHeight();

    //отрисовка полоски опыта
    g.setColor(Color.black);
    g.drawRect(0, height - 15, width, 15);
    g.setColor(Color.magenta);
    part = ((double) hero.getXP()) / hero.getToLvlXP();
    g.fillRect(0, height - 14, (int) (width * part), 13);
    g.setColor(Color.black);
    width = game.getWidth() / 20;
    for (int i = 1; i < 20; i++)
      g.drawLine(i * width, height - 15, i * width, height);
  }


  public void switchShowHeroStats()
  {
    showHeroStats = !showHeroStats;
  }
}
