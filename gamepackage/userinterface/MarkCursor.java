package gamepackage.userinterface;

import com.golden.gamedev.object.*;
import gamepackage.ToolSet;

public class MarkCursor extends Sprite
{
  public MarkCursor()
  {
    super(ToolSet.getInstance().getImageTool().getImage("resources/mark.png"));
  }


  public void update(int mouseX, int mouseY)
  {
    setLocation(mouseX - 25, mouseY - 25);
  }
}
