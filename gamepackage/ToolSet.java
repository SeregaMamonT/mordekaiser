package gamepackage;

import com.golden.gamedev.Game;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: salexetovich
 * Date: 29.08.13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
public class ToolSet
{
  private static ToolSet toolSet;

  private Game game;
  private Random random;
  private ImageTool imageTool;


  private ToolSet(Game game)
  {
    this.game = game;
  }


  static void init(Game newGame)
  {
    toolSet = new ToolSet(newGame);
  }


  public static ToolSet getInstance()
  {
    return toolSet;
  }


  public Game getGame()
  {
    return game;
  }


  public Random getRandom()
  {
    if (random == null) {
      random = new Random();
    }
    return random;
  }


  public ImageTool getImageTool()
  {
    if (imageTool == null) {
      imageTool = new ImageTool();
    }
    return imageTool;
  }
}
