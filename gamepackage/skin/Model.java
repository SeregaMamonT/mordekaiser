package gamepackage.skin;

import java.awt.image.BufferedImage;

import gamepackage.ToolSet;

import java.io.*;

public class Model
{
  private BufferedImage[] images;
  private final ModelType modelType;
  private int centerX, centerY;


  public Model(ModelType modelType, String dirname) throws IOException
  {
    this.modelType = modelType;

    String modelCode = modelType.getCode();
    BufferedReader br = new BufferedReader(new FileReader(dirname + "/" + modelCode + ".model"));

    String[] words = br.readLine().split(" ");
    int cols = Integer.parseInt(words[0]);
    int rows = Integer.parseInt(words[1]);
    images = ToolSet.getInstance().getImageTool().getImages(dirname + "/" + modelCode + ".png", cols, rows);

    words = br.readLine().split(" ");
    centerX = Integer.parseInt(words[0]);
    centerY = Integer.parseInt(words[1]);

    br.close();
  }


  public BufferedImage[] getImages()
  {
    return images;
  }


  public int getCenterX()
  {
    return centerX;
  }


  public int getCenterY()
  {
    return centerY;
  }


  public ModelType getModelType()
  {
    return modelType;
  }

}
