package gamepackage.skin;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author User
 */
public class Skin
{
  private Map<ModelType, Model> modelMap = new HashMap<>();

  private final String name;


  public Skin(String name, String dirname) throws IOException
  {
    this.name = name;
    loadModels(dirname);
  }


  private void loadModels(String skinDir) throws IOException
  {
    File[] modelDirectories = new File(skinDir).listFiles();
    assert modelDirectories != null : String.format("Skin '%s' has no defined models!", skinDir);
    for (File modelDirectory : modelDirectories) {
      ModelType modelType = ModelType.getInstance(modelDirectory.getName());
      modelMap.put(modelType, new Model(modelType, skinDir + "/" + modelDirectory.getName()));
    }
  }


  public Model getModel(ModelType modelType)
  {
    return modelMap.get(modelType);
  }


  public String getName()
  {
    return name;
  }
}
