package gamepackage.skin;

import java.util.HashMap;
import java.util.LinkedList;

import java.io.IOException;
import java.util.Map;

/**
 * @author User
 */
public class SkinFactory
{
  private static Map<String, Skin> skins = new HashMap<>();


  public static Skin getSkin(String name)
  {
    if (!skins.containsKey(name)) {
      try {
        skins.put(name, new Skin(name, "resources/skins/" + name));
      }
      catch (IOException e) {
        throw new RuntimeException(String.format("Not skin '%s' found!", name));
      }
    }
    return skins.get(name);
  }

}