package gamepackage.skin;

/**
 * User: salexetovich
 * Date: 21.07.14
 */
public enum ModelType
{
  LEFT_MOVE       ("leftmove"),
  RIGHT_MOVE      ("rightmove"),
  LEFT_ATTACK     ("leftattack"),
  RIGHT_ATTACK    ("rightattack"),
  PROJECTILE      ("projectile"),
  RESPAWN         ("respawn");

  private String code;

  ModelType(String code)
  {
    this.code = code;
  }


  public String getCode()
  {
    return code;
  }


  public static ModelType getInstance(String code)
  {
    for (ModelType modelType : values()) {
      if (modelType.getCode().equals(code)) {
        return modelType;
      }
    }
    return null;
  }


  public ModelMode getMode()
  {
    return code.contains("attack") ? ModelMode.ATTACK : ModelMode.MOVE;
  }
}
