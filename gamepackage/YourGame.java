package gamepackage;

import java.awt.*;
import java.awt.event.*;

import com.golden.gamedev.*;
import com.golden.gamedev.object.*;
import gamepackage.gameobject.creature.Creature;
import gamepackage.gameobject.creature.CreatureFactory;
import gamepackage.gameobject.creature.Hero;
import gamepackage.map.ClientGameField;
import gamepackage.technical.NumericUtil;
import gamepackage.technical.Point;
import gamepackage.userinterface.MarkCursor;
import gamepackage.userinterface.ScreenInterface;

import java.io.*;

public class YourGame extends Game
{
  Hero hero;
  ClientGameField field;
  ScreenInterface screenInterface;
  MarkCursor cursor;
  Creature underMouse;

  public void initResources()
  {
    ToolSet.init(this);
    GameFont font = fontManager.getFont(getImages("resources/font1.png", 20, 3), " !/           .,0123" + "456789:   -? ABCDEFG" + "HIJKLMNOPQRSTUVWXYZ ");

    screenInterface = new ScreenInterface(font);
    field = new ClientGameField(1);

    hero = CreatureFactory.getHero("mordekaiser", "mordekaiser");
    hero.getAnimationTimer().setDelay(500);
    field.setHero(hero);

    cursor = new MarkCursor();
    hideCursor();
  }


  public void update(long elapsedTime)
  {
    cursor.update(getMouseX(), getMouseY());
    field.update(elapsedTime);
    hero.update(elapsedTime);

    underMouse = field.onLocation(getMouseX() + field.getX(), getMouseY() + field.getY());

    if (underMouse == hero)
      underMouse = null;

    switch (bsInput.getKeyPressed()) {
      case KeyEvent.VK_C:
        screenInterface.switchShowHeroStats();
        break;
      case KeyEvent.VK_ESCAPE:
        game.getGame().finish();
        break;
    }


    if (bsInput.isMouseDown(MouseEvent.BUTTON1)) {

      if (underMouse != null && hero.canAttack(underMouse))
        hero.attack(underMouse);
      else {
        Point direction = NumericUtil.direction(hero.getScreenX(), hero.getScreenY(), getMouseX(), getMouseY());
        double cos = direction.getX();
        double sin = direction.getY();
        hero.setDirection(cos, sin);
        if (field.isMovingAllowed(hero, cos, sin)) {
          hero.move(cos, sin);
        }
      }
    }
  }


  public void showMessages(Graphics2D g)
  {
    screenInterface.drawHealth(g, underMouse);
    screenInterface.drawHeroPanel(g, hero);
    screenInterface.showHeroStats(g, hero);
  }


  public void render(Graphics2D g)
  {
    field.render(g);
    hero.render(g);
    cursor.render(g);
    showMessages(g);
  }

  static GameLoader game;


  public static void main(String[] args) throws IOException
  {
    game = new GameLoader();
    game.setup(new YourGame(), new Dimension(1024, 600), false);
    //game.setup(new YourGame(), new Dimension(640, 480), false);
    game.start();
  }
}
